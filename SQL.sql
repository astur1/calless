﻿DROP DATABASE IF EXISTS calles;
CREATE DATABASE IF NOT EXISTS calles;
USE calles;
CREATE TABLE zonaurbana (
   nzona varchar (30),
   categoria varchar (30),
  ncasa int,
  PRIMARY KEY (nzona, ncasa),
  CONSTRAINT zonacasa FOREIGN KEY (ncasa) REFERENCES casa (ncasa)
);
CREATE TABLE bloquecasa (
   npisos int,
  calle varchar (40),
  nbloque int,
  planta int,
  puerta int,
  PRIMARY KEY (calle, nbloque, planta, puerta),
  CONSTRAINT blopi FOREIGN KEY (planta) REFERENCES piso (planta),
  CONSTRAINT blopis FOREIGN KEY (puerta) REFERENCES piso (puerta)
);
CREATE TABLE casa (
   ncasa int,
  mcasa int,
  PRIMARY KEY (ncasa)
);
CREATE TABLE piso (
  mpiso int,
  planta int,
  puerta int,
  PRIMARY KEY (planta, puerta),
);
CREATE TABLE poseecasa (
  dni int,
  nombrepersona varchar (30),
  edad int,
  ncasa int,
  nzona varchar (40),
  PRIMARY KEY (dni, ncasa, nzona),
  CONSTRAINT personaca FOREIGN KEY (ncasa) REFERENCES zonaurbana (ncasa),
  CONSTRAINT personacas FOREIGN KEY (nzona) REFERENCES zonaurbana (nzona),
);
CREATE TABLE poseepiso (
  dni int,
  planta int,
  puerta int,
  calle varchar (40),
  nbloque int,
  PRIMARY KEY (dni, planta, puerta, calle, nbloque),
  CONSTRAINT persopiso FOREIGN KEY (dni) REFERENCES persona (dni),
  CONSTRAINT persopis FOREIGN KEY (planta) REFERENCES bloquecasa (planta),
  CONSTRAINT persopi FOREIGN KEY (puerta) REFERENCES bloquecasa (puerta),
  CONSTRAINT persop FOREIGN KEY (calle) REFERENCES bloquecasa (calle),
  CONSTRAINT persopio FOREIGN KEY (nbloque) REFERENCES bloquecasa (nbloque)
);
CREATE TABLE persona (
dni  int,
npersona varchar (30),
  edad int,
  ncasa int,
  nzona int,
  planta int,
  puerta int,
  calle varchar (40),
  nbloque int,
PRIMARY KEY ( dni, ncasa, nzona, calle, nbloque, planta, puerta),
  CONSTRAINT perszon FOREIGN KEY (nzona) REFERENCES zonaurbana (nzona),
  CONSTRAINT perszo FOREIGN KEY (ncasa) REFERENCES zonaurbana (ncasa),
  CONSTRAINT erszona FOREIGN KEY (calle) REFERENCES bloquecasa (calle),
  CONSTRAINT rszona FOREIGN KEY (nbloque) REFERENCES bloquecasa (nbloque),
  CONSTRAINT perzona FOREIGN KEY (planta) REFERENCES bloquecasa (planta),
  CONSTRAINT peszona FOREIGN KEY (puerta) REFERENCES bloquecasa (puerta)
);